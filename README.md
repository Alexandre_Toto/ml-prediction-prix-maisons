# MODEL DE PREDICTION DU PRIX DE MAISON

## Introduction
- Ce projet est un projet de prédiction du prix de maison qui devait être réalisé par ces propres soins. 
- Il est réalisé dans le cadre de la formation de Data Scientist.
- Les data sont issues du site Kaggle.

## Prérequis
- Python 3.10
- Jupyter Notebook
- Pandas
- Numpy
- Matplotlib
- Seaborn
- Scikit-learn

## Installation
- Cloner le projet sur votre machine
- Installer les librairies citées ci-dessus
- Lancer le notebook
- Lancer les cellules une à une
- Enjoy !

## Auteur
- Alexandre Toto (Ingénieur en développement web & Data engineer)